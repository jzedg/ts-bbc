# Think Sumo Laravel Base
### FIRST TIME INSTALLERS
- download Python 2.7.11: [https://www.python.org/downloads/release/python-2710/]
- add python to PATH
- npm config set python python2.7
- npm config set msvs_version 2015 --global

### DO THIS AT THE START
```sh
composer update
npm install
bower install
gulp copy
php artisan migrate --seed 
```

### FAQs
- API is auto-generated in /Http/Controllers/Api (need to copy paste routes)
- Pag may error sa seed at migrations na gawa ng ibang dev, try "composer dump-autoload"
- Use Factory to make fake values; can also be used in a Seeder

### Release notes
#### 1.2.0
1. S3 access key and id in default .env no longer has global s3 permission.  To create a project specific s3 key/secret, refer to this documentation
https://3.basecamp.com/3368673/buckets/1109201/documents/1196827008

#### 1.1.1
1. GA code retrieval added in Acme::General. Will use site-options "analytics-script" -- For BA's Just PUT the GA ID here configurable in site options. https://bitbucket.org/tsumo-bucket/laravel-base/commits/491faf3e848017064b05f8357c80c1020990e60d
2. Added new site option in seeder -- For Devs, use this email address for Contact Us forms
https://bitbucket.org/tsumo-bucket/laravel-base/commits/3baccee61debf017757cb3d9477158e53b2cc156
3. Fixed parsely for multiple forms. -- For Devs, you can now have multiple parsely forms in admin backend.
https://bitbucket.org/tsumo-bucket/laravel-base/commits/5ad39d45bdbad5b517d5da9d6dc708968907f069

#### 1.0.0 
Initial Version
