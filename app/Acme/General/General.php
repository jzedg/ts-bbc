<?php 

namespace Acme\General;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\View;

use App\Option;
use App\Email;
use Mail;
use Carbon;

class General extends Mail
{	

	public function get_ga_code(){
		$option = Option::whereSlug('analytics-script')->first();
		$response = ($option) ? $option->value: 'TBD';
		return $response;
	}
	
	public function slug($value,$id)
	{
		$slug = str_slug($value."-".$id);
		return $slug;
	}

	public function checkPermission($children,$ids,$curParentId,$data){
		foreach($children as $c){
			if($c->parent==0){

				if(count($c->children)>0){
					echo '<li><input type="checkbox" id="func-'.$c->id.'" name="ids[]" value="'.$c->id.'" '.(in_array($c->id,$data) ? "checked":"").'><label for="func-'.$c->id.'" class="parent header">';
					echo $c->name;
					echo '</label></li>';
					echo '<ul class="parent permission-list">';
					$this->checkPermission($c->children,$ids,$c->id,$data);
					echo '</ul>';

				}else{
					echo '<li><input type="checkbox" name="ids[]" value="'.$c->id.'" '.(in_array($c->id,$data) ? "checked":"").'><label class="parent header">';
					echo $c->name;
					echo '</label></li>';
				}
			}
			else if(!in_array($c->id,$ids)){

				if(count($c->children)>0){
					echo '<li><input type="checkbox" id="func-'.$c->id.'" name="ids[]" value="'.$c->id.'" '.(in_array($c->id,$data) ? "checked":"").'><label for="func-'.$c->id.'" class="parent header">';
					echo $c->name;
					echo '</label></li>';
					echo '<ul class="permission-list">';
					$this->checkPermission($c->children,$ids,$c->id,$data);
					echo '</ul>';

				}else{
					echo '<li><input type="checkbox"  id="func-'.$c->id.'" name="ids[]" name="ids[]" value="'.$c->id.'" '.(in_array($c->id,$data) ? "checked":"").'><label for="func-'.$c->id.'" class="parent header">';
					echo $c->name;
					echo '</label></li>';
				}
			}
			else if(in_array($c->id,$ids) && $c->parent==$curParentId){

				if(count($c->children)>0){
					echo '<li><input type="checkbox" id="func-'.$c->id.'" name="ids[]" value="'.$c->id.'" '.(in_array($c->id,$data) ? "checked":"").'><label for="func-'.$c->id.'" class="parent header">';
					echo $c->name;
					echo '</label></li>';
					echo '<ul class="permission-list">';
					$this->checkPermission($c->children,$ids,$c->id,$data);
					echo '</ul>';

				}else{
					echo '<li><input type="checkbox" id="func-'.$c->id.'" name="ids[]" value="'.$c->id.'" '.(in_array($c->id,$data) ? "checked":"").'><label for="func-'.$c->id.'" class="parent header">';
					echo $c->name;
					echo '</label></li>';
				}
				continue;

			}
		}

	}
}